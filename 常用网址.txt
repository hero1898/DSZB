1、给英文不好的朋友

GitHub 中文化插件 🔰https://greasyfork.org/zh-CN/scripts/435208

Github 增强-高速下载 🔰https://greasyfork.org/zh-CN/scripts/412245

2、Github RAW 加速服务

源码 https://gitcdn.top/https://github.com/用户名/仓库名/raw/main/接口文件

香港 https://raw.iqiq.io/用户名/仓库名/main/接口文件

新加坡 https://raw.kgithub.com/用户名/仓库名/main/接口文件

日本

https://fastly.jsdelivr.net/gh/用户名/仓库名@main/接口文件

https://cdn.staticaly.com/gh/用户名/仓库名/main/接口文件

https://raw.fastgit.org/用户名/仓库名/main/接口文件

韩国

https://ghproxy.com/https://raw.bgithub.xyz/用户名/仓库名/main/接口文件

https://ghproxy.net/https://raw.bgithub.xyz/用户名/仓库名/main/接口文件

https://gcore.jsdelivr.net/gh/用户名/仓库名@main/接口文件

https://raw.githubusercontents.com/用户名/仓库名/main/接口文件

3，Github 静态加速

https://cdn.staticaly.com/gh/用户名/仓库名/main/接口文件

https://cdn.jsdelivr.net/gh/用户名/仓库名@main/接口文件

https://purge.jsdelivr.net/gh/用户名/仓库名@main/接口文件

4、EGP源

http://epg.51zmt.top:8000/e.xml

https://epg.112114.xyz/pp.xml

以下为地址

epg-DIYP接口1：(hhttp://epg.51zmt.top:8000/api/diyp/)

epg-DIYP接口2：(http://diyp.112114.xyz/)

epg-DIYP接口3：(https://epg.hicloud.co/epg.php/)

epg1：(https://epg.sec.st/epg.php/)

epg3：(https://epg.pm/)

epg4：(http://n33426t756.wicp.vip/diyp/epg.php/)

epg5：(http://www.diyp.top/diyp/epg.php/)

总epg：(http://epg.51zmt.top:8000/e.xml/)

央视及各省卫视epg：(http://epg.51zmt.top:8000/cc.xml/)

地方及数字付费epg：(http://epg.51zmt.top:8000/difang.xml/)

港澳台及海外epg：(http://epg.51zmt.top:8000/gat.xml/)

epg6：(http://124.223.212.38:83/)

epg7：(https://epg.112114.xyz/)

超级直播

Xml格式

Xml格式

5、开源仓库

https://github.com/

https://gitlab.com/

https://gitee.com/

https://coding.net/

https://gitcode.net/

https://gitea.com/ 仓库名是 mao,tvbox,box,tv等类似的，有几率出现 1.删除仓库 2.删除用户 3.封禁账户 4.黑名单

https://agit.ai/

https://notabug.org/

6、短地址平台

（1）https://short.io

（2）http://88d.cn

（3）https://77url.com

（4）https://suowo.cn

（5）https://6du.in

（6）https://www.urlc.cn

（7）https://59z.cn

（8）https://suowo.cn

（9）https://0a.fit/

（10）https://www.urlc.cn/

7、TVBox各路大佬配置（排名不分先后）：

（1）唐三：https://hutool.ml/tang

（2）Fongmi：https://raw.fastgit.org/FongMi/CatVodSpider/main/json/config.json

（3）俊于：http://home.jundie.top:81/top98.json

（4）饭太硬：http://饭太硬.ga/x/o.json

（5）霜辉月明py：https://ghproxy.com/raw.bgithub.xyz/lm317379829/PyramidStore/pyramid/py.json

（6）小雅dr：http://drpy.site/js1

（7）菜妮丝：https://tvbox.cainisi.cf

（8）神器：https://神器每日推送.tk/pz.json

（9）巧技：http://pandown.pro/tvbox/tvbox.json

（10）刚刚：http://刚刚.live/猫

（11）吾爱有三：http://52bsj.vip:98/0805

（12）潇洒：https://download.kstore.space/download/2863/01.txt

（13）佰欣园：https://ghproxy.com/https://raw.bgithub.xyz/chengxueli818913/maoTV/main/44.txt

（14）胖虎：https://notabug.org/imbig66/tv-spider-man/raw/master/配置/0801.json

（15）云星日记：https://maoyingshi.cc/tiaoshizhushou/1.txt

（16）Yoursmile7：https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json

（17）BOX：http://52bsj.vip:81/api/v3/file/get/29899/box2.json?sign=3cVyKZQr3lFAwdB3HK-A7h33e0MnmG6lLB9oWlvSNnM%3D%3A0

（18）哔哩学习：http://52bsj.vip:81/api/v3/file/get/41063/bili.json?sign=TxuApYZt6bNl9TzI7vObItW34UnATQ4RQxABAEwHst4%3D%3A0

（19）UndCover：https://raw.bgithub.xyz/UndCover/PyramidStore/main/py.json

（20）木极：https://pan.tenire.com/down.php/2664dabf44e1b55919f481903a178cba.txt

（21）Ray：https://dxawi.github.io/0/0.json

（22）甜蜜：https://kebedd69.github.io/TVbox-interface/py甜蜜.json

（23）52bsj：http://52bsj.vip:81/api/v3/file/get/29899/box2.json?sign=3cVyKZQr3lFAwdB3HK-A7h33e0MnmG6lLB9oWlvSNnM%3D%3A0

（24）肥猫：http://我不是.肥猫.love:63

8、随机轮换壁纸：

（风景1）https://bing.img.run/rand.php

（风景2）https://api.lyiqk.cn/scenery

（背景图）http://www.kf666888.cn/api/tvbox/img

（性感美女1）https://api.lyiqk.cn/api

（性感美女2）https://api.lyiqk.cn/sexylady

（性感美女3）http://api.btstu.cn/sjbz/?lx=meizi

（性感美女4）https://api.lyiqk.cn/mjx

（清纯美女）https://api.lyiqk.cn/purelady

（menhera酱）https://api.lyiqk.cn/menhera

（每日一图）https://api.lyiqk.cn/bing

（随机图）https://api.yimian.xyz/img

（初音未来）https://api.lyiqk.cn/miku

（动漫图）https://img.xjh.me/random_img.php

（英雄联盟）https://api.lyiqk.cn/lol

（王者荣耀）https://api.lyiqk.cn/pvp

（二次元动漫）https://api.lyiqk.cn/acg

（动漫图下载）https://www.dmoe.cc/random.php

9、工具

（1）文本处理： http://www.txttool.com/


1.2 直播
(1) Televizo(推荐)
适用:安卓手机/TV
官网:https://televizo.net/
建议版本:1.9.3.21 Premium
下载:https://www.applnn.cc/18580.html
(3) 我的电视(内置源)
适用:安卓手机/TV
下载:https://lyrics.run/my-tv.html
(4) MPC-HC
适用:Windows PC
官网:https://mpc-hc.org/ (不再更新)
第三方:https://github.com/clsid2/mpc-hc/releases
(5) APTV
适用:Apple
官网:https://github.com/Kimentanm/aptv
App Store:https://apps.apple.com/cn/app/id1630403500
(6) GOTV
适用:iPhone/iPad
App Store:https://apps.apple.com/cn/app/id1271283728
(7) M3U8
http://zqjy.info/tv/
适用:浏览器
1.3 系统
适用:安卓TV
(1) 文件管理
小白文件管理器:http://www.juwanhezi.com/item/247
(2) ADB
Termux:https://termux.dev/cn/
>更新apt:apt update && apt upgrade
>安装adb:apt install android-tools
>获取存储权限:termux-setup-storage
>连接电视终端(提前打开ADB调试):adb connect 192.168.1.108
>安装软件:
cd /storage/emulated/0/Download/quark
adb install xxx.apk
(3) 桌面
Emotn UI:https://app.emotn.com/ui/
使用方法:
>>安装Emotn UI
adb install emotnui.apk
>>禁用
禁用系统桌面:
adb shell pm disable-user "com.mitv.tvhome"
禁用系统桌面更新:
adb shell pm disable-user "com.xiaomi.mitv.upgrade"
(4) 设置
> 安装Settings.apk和Starter.apk
下载:https://www.yuu.ink/article/136/
(5) 应用商店
美家市场:https://www.mjapk.com/
>> 删除小米应用商店
adb shell pm uninstall --user 0 com.xiaomi.mitv.appstore
(6) 浏览器
Emotn Browser:https://app.emotn.com/browser/
1.4 托管平台
# 国内外主流的 Git 代码托管平台
https://www.cnblogs.com/jetsung/p/git-service.html
(1) github
https://bgithub.xyz/
(2) Codeberg(推荐)
https://codeberg.org/
(3) netlify
https://app.netlify.com/
(4) Vercel
https://vercel.com/
1.5 应用市场
(1) 分享迷:https://www.fenxm.com/tv
(2) 聚玩盒子:http://www.juwanhezi.com/

2. 直播源
2.1 个人收集
(1) 电视直播
https://huangsuming.github.io/iptv/list/tvlist.txt
https://huangsuming.codeberg.page/list/tvlist.txt
https://iptv-huangsuming.netlify.app/list/tvlist.txt
https://iptv-huangsuming.vercel.app/list/tvlist.txt
(2) 电台直播
https://huangsuming.github.io/iptv/list/radio.txt
https://huangsuming.codeberg.page/list/radio.txt
https://iptv-huangsuming.netlify.app/list/radio.txt
https://iptv-huangsuming.vercel.app/list/radio.txt
2.2 播放列表
(1) AdultIPTV(nsfw)
http://adultiptv.net/chs.m3u
http://adultiptv.net/videos.m3u8
(2) Free-TV
https://bgithub.xyz/Free-TV/IPTV
(3) iptv-org
https://iptv-org.github.io/
2.3 Github
> 搜索: iptv stars:>50 path:*.m3u8 pushed:>2024-01-01 
https://bgithub.xyz/fenxp/iptv
https://bgithub.xyz/Kimentanm/aptv
https://bgithub.xyz/YanG-1989/m3u
https://bgithub.xyz/YueChan/Live
2.4 在线网站
(1) CNR(广播)
https://www.cnr.cn/gbzb/
(2) Radios(广播)
https://instant.audio/
(3) Radio5(广播)
https://radio5.cn/
(4) Sao(广播)
https://sao.fm
(5) Tvzb(论坛)
https://www.tvzb.com/
(6) imxd(论坛)
https://discuz.mxdyeah.top/
(7) 爱TV
http://www.iptv8.top/
(7) 好趣网(电视)
http://tv.haoqu99.com/
(8) 123iptv
https://www.123iptv.tv/
(9) 66直播(电视)
http://www.66zhibow.com/
2.5 活跃用户
https://www.right.com.cn/forum/?449509
2.6 订阅源
https://yibababa.com/tv/list.html
2.7 直播源说明
(1)移动
#黑龙江移动:hl
ottrrs.hl.chinamobile.com
#黑龙江鸡西移动:0453
39.134.65.149
39.134.65.164
39.134.65.166
39.134.65.173
39.134.65.175
39.134.65.177
39.134.65.179
39.134.65.181
39.134.65.183
39.134.65.208
39.134.66.110
39.134.66.2
39.134.66.46
39.134.66.48
39.134.66.66
39.134.67.226
#江西移动:
hwrr.jx.chinamobile.com:8080
(2)地方传媒
#河南大象融媒体:dxhmt.cn
如:长垣综合,http://live.dxhmt.cn:9081/tv/10728-1.m3u8
#陕西秦岭云:sxbc
如:CCTV13,http://223.109.210.41/zycfcdn.gdwlcloud.com/PLTV/88888888/224/3221226062/index.m3u8
#湖南华声在线:voc
如:长沙综合,https://liveplay-srs.voc.com.cn/hls/tv/203_10cdf5.m3u8
(3)酒店源
#公明新围科源网络(不推荐,不流畅):gmxw
(高清HD1)如:广东影视,http://gmxw.7766.org:808/hls/6/index.m3u8
(高清HD2)如:吉林卫视,http://183.11.239.36:808/hls/10/index.m3u8
#智慧光迅:zhgxtv
(标清SD)如:环球奇观,http://59.44.192.82:65000/hls/35/index.m3u8
(高清HD1)如:河北都市,http://111.11.98.205:9900/hls/21/index.m3u8
(高清HD2)如:CCTV10,http://183.196.25.171:808/hls/10/index.m3u8
#点量TV:dolit
(标清SD)如:CCTV1,http://222.241.154.37:9901/tsfile/live/23001_1.m3u8
(高清HD1)如:CCTV1,http://113.57.103.216:9000/tsfile/live/0001_1.m3u8
(高清HD2)如:CCTV1,http://117.141.149.101:4431/tsfile/live/faacts/1010_1.m3u8
(4)老挝
#laotv(如:凤凰中文,https://edge2.laotv.la/live/PhxChinese/index.m3u8)
#laos(如:CCTV-5,http://103.95.24.37:880/CCTV-5.m3u8)

3. 接口
3.1 TVBOX接口
(1) 聚玩盒子:
https://juwanhezi.com/other/jsonlist
(2) Potplayer:
https://www.potplay.net/zhiboyuan
(3) 看电视呗:
http://tvbox6.com/tv.txt
3.2 EPG
https://epg.112114.eu.org/
http://epg.51zmt.top:8000/e.xml
https://live.fanmingming.com/e.xml
https://epg.imxd.top/download/all-mxdyeah.xml
https://raw.bgithub.xyz/Troray/IPTV/main/tvxml.xml
3.3 logo
> 台标
https://live.imxd.top/logo/list.txt
https://bgithub.xyz/lqtv/logo/blob/main/README.md
https://bgithub.xyz/wanglindl/TVlogo/blob/main/README.md